﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using MSharp.Framework;
using System.Diagnostics;
using System.Reflection;
using System.Collections.Generic;

namespace Msharp_cleanup_app_project
{
    class Program
    {


        static void Main()
        {
            bool removeAll = Config.Get("RemoveAll")?.ToLower() == "true";
            var startime = LocalTime.Now;
            var drive = new DriveInfo(Config.Get("Msharp.IDE.Folder"));
            var startFreeSpace = drive.AvailableFreeSpace;

            ShowCredits();

            if (ReadyToRun())
            {
                if (removeAll)
                    Console.Write("\nRemoveAll config set\n");

                DeleteAllButLatestFolders(Config.Get("Msharp.MetadataFolder"), removeAll);

                DeleteAllButLatestFolders(GetAppDataTempFolder(), removeAll);
                DropAllButLatestDatabase(removeAll);

                Console.Write("\n\nFinished");

                var timeUsed = LocalTime.Now.Subtract(startime);

                var cleanedUpSpace = drive.AvailableFreeSpace - startFreeSpace;

                if (timeUsed.TotalSeconds > 1)
                    Console.Write("\n\nThis cleaning took " + timeUsed.TotalMinutes.ToString("N0") + " minutes and " + timeUsed.Seconds + " seconds");

                var spaceToLog = ShowCleanedUpSpace(cleanedUpSpace, drive);
                if (spaceToLog.HasValue())
                    AddToLog(spaceToLog, timeUsed);
            }

            KeyToExit();
        }

        private static void KeyToExit()
        {
            while (Console.KeyAvailable)
            {
                Console.ReadKey(intercept: true);
            }
            Console.Write("\n\n *** Press any key to exit ***");
            Console.ReadKey();
        }

        private static string GetAppDataTempFolder()
        {
            return Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData).Replace("\\Roaming", "") + Config.Get("User.Appdata.Msharp.Tempfolderpath");
        }

        private static void AddToLog(string savedSpace, TimeSpan timeUsed)
        {
            var filepath = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName) + "\\MsharpCleanupLog.txt";
            var logline = LocalTime.Now + " - Computer name: " + Environment.MachineName + " - Space cleaned: " + savedSpace + " - Cleaning time: " +
                 timeUsed.TotalMinutes.ToString("N0") + " minutes, " + timeUsed.Seconds + " seconds";

            var file = filepath.AsFile();
            if (!file.Exists())
                file.Create();
            file.AppendLine(logline);
        }

        private static string ShowCleanedUpSpace(long cleanedUpSpace, DriveInfo drive)
        {
            string cleandUpSpace = null;
            if (cleanedUpSpace > 1024 * 1024 * 1024)
            {
                cleandUpSpace = (Convert.ToDecimal(cleanedUpSpace) / 1024 / 1024 / 1024).ToString("N") + " GB ";
            }
            else
            {
                cleandUpSpace = (Convert.ToDecimal(cleanedUpSpace) / 1024 / 1024).ToString("N") + " MB ";
            }
            if (cleanedUpSpace > 0)
            {
                Console.Write("\n\nThis cleaning freed up " + cleandUpSpace + " on your " + drive.Name + " drive.");
                return cleandUpSpace;
            }
            return null;

        }

        private static bool ReadyToRun()
        {
            if (File.Exists(Config.Get("Msharp.IDE.Folder") + "\\CurrentProject.xml"))
            {
                Console.Write("\n\tProjects are still open. \n\tClose them and run this app again");
                return false;
            }

            try
            {
                if (!Directory.Exists(Config.Get("Msharp.MetadataFolder")))
                {
                    Console.Write("\n\tNo M# metafolder found.");
                    return false;
                }
            }
            catch
            {
                return false;
            }
            return true;
        }

        private static void DeleteAllButLatestFolders(string projectFolderPath, bool removeAll)
        {
            if (!projectFolderPath.AsDirectory().Exists())
                return;

            Console.Write("\nLooking for project folders to clean up in \n   " + projectFolderPath);

            if (Directory.GetDirectories(projectFolderPath).None())
                Console.Write("\n   No project folders found\n");

            foreach (var directory in Directory.GetDirectories(projectFolderPath))
            {
                var projectName = directory.Split('\\').Last();
                Console.Write("\n   Project folder found for: " + projectName + " \n\tChecking...");
                var latestSubDirectory = Directory.GetDirectories(directory).Max();
                var count = 0;

                var toDelete = (Directory.GetDirectories(directory).Count());
                if (!removeAll)
                    toDelete--;

                if (toDelete > 0)
                {
                    Console.Write("\n\tThere are " + toDelete + " directories to delete\n\tWorking...");
                    foreach (var subdirectory in Directory.GetDirectories(directory))
                    {
                        if (subdirectory != latestSubDirectory || removeAll)
                        {
                            count++;
                            Directory.Delete(subdirectory, recursive: true);
                            if (count > 0 && count % 25 == 0)
                            {
                                Console.Write("\n\t" + count + " / " + toDelete + " directories deleted in the " + projectName + " project.");
                            }
                        }
                    }
                    Console.Write("\n\tDeleted " + count + " temp folders for the " + directory.Split('\\').Last() + " project.");
                }
                else
                {
                    Console.Write("\n\tNo need to delete any subfolders for this project");
                }
                Console.Write("\n");
            }
        }

        private static void DropAllButLatestDatabase(bool removeAll)
        {
            Console.Write("\nLooking for databases to clean up");
            var dbConnection = new SqlConnection(Config.Get("Msharp.Database.ConnectionString"));

            try
            {
                var selectDbNamesCommand = new SqlCommand("SELECT name FROM master.dbo.sysdatabases where name like '@Meta_%'", dbConnection);

                dbConnection.Open();

                var reader = selectDbNamesCommand.ExecuteReader();
                var dataTable = new DataTable();
                dataTable.Load(reader);
                var names = dataTable.GetRows().Select(r => r.ItemArray.ToString(""));

                var toDelete = new List<string>();

                if (removeAll)
                    toDelete.AddRange(names);
                else
                    toDelete.AddRange(names.GroupBy(n => n.Split('(').First()).SelectMany(g => g.Select(n => n).OrderByDescending(n => n).Skip(1)));


                if (toDelete.Any())
                {
                    Console.Write("\n\t" + toDelete.Count() + " can be cleaned.\n\tCleaning...");

                    toDelete.Do(db =>
                        {
                            var sqlCommandText = @"DROP DATABASE """ + db + @"""";
                            var sqlCommand = new SqlCommand(sqlCommandText, dbConnection);
                            sqlCommand.ExecuteNonQuery();
                        });
                    Console.Write("\n\t" + toDelete.Count() + " databases cleaned");
                }
                else
                {
                    Console.Write("\n\tNo databases that could be cleaned found");
                }
            }
            catch (SqlException ex)
            {
                Console.Write("\n\n\tERROR: Can't connect to database. \n\tCheck connection string in config file.\n\n");
            }

            dbConnection.Close();
        }

        private static void ShowCredits()
        {
            var assembly = Assembly.GetExecutingAssembly();
            var fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
            string version = fvi.FileVersion;


            Console.Write("\n  **************************************" +
                          "\n  *                                    *" +
                          "\n  *  M# metaCleanUp                    *" +
                          "\n  *                                    *" +
                                  "\n  *  Version: " + version + new string(' ', 25 - version.Length) + "*" +
                          "\n  *  By: P.C. Kofstad                  *" +
                          "\n  *                                    *" +
                          "\n  **************************************\n");
        }
    }

}
