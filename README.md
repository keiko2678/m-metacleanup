M# Cleanup tool

By: p.c. Kofstad

- It cleans up all but the latest of M# metadata folders and databases.
- It will not start if you have any M# projects open. 
- It can take a bit of time to run if you have not done a cleanup in a while. 
- It can clean up several GB on you C drive if you have not run it in a while. 

Free to use, free to copy. Please acknowledge if the code is reused. 